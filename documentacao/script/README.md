# Script de Documentação do banco IGEO

__Python:__ 3.7.3p

Dependencias:

- Psycopg2-binary - `pip install psycopg-binary`
- [Markdown](https://www.markdownguide.org/) - `pip install marckdow`
- [Mkdocs](https://www.mkdocs.org/) - `pip install mkdocs`
- Tema Mkdocs: [Material](https://squidfunk.github.io/mkdocs-material/) - `pip install mkdocs-material`

## parametros.ini

Arquivo que recebe os parametros que o script irá usar.

- `[caminho_base]` - Recebe o caminho da pasta `docs` do repositorio que o script irá usar para gerar a documentação. Esse caminho deverá ser alterado de acordo com a máquina de cada um.
- `[schema_exclude]` - Recebe a lista de schemas que o script irá ignorar na hora de buscar para gerar a documentação.

## Usage

__Comando:__ `python gera_documentacao.py`
