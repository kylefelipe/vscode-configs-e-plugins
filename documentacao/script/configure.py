# -*- coding: utf-8 -*-

from configparser import ConfigParser
 
def config(section):
    """ This parser takes settings from a file .ini"""
    
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read('parametros.ini')
 
    # get section, default to postgresql
    conf = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            conf[param[0]] = param[1]
    else:
        raise Exception(f'Seção {section} não encontrada no arquivo parametros.ini.')
 
    return conf
