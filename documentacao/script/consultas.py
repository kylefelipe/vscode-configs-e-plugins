# -*- coding: utf-8 -*-

# SQLS

getSchemas = """SELECT schema_name
FROM information_schema.schemata
WHERE schema_name NOT IN ({schemas_exclude})
ORDER BY schema_name;"""

schema_owner = """SELECT schema_name, schema_owner
FROM information_schema.schemata
WHERE schema_name = '{table_schema}'
ORDER BY schema_name;"""

schemaDescription = """SELECT "nsp"."nspname","pgd"."description"
FROM "pg_description" "pgd"
JOIN "pg_catalog"."pg_namespace" "nsp" ON "pgd"."objoid" = "nsp".oid
WHERE "nsp"."nspname" = '{table_schema}';"""

# Gerando lista de tabelas, views e view materializada

schemaTables = """SELECT "table_name"
FROM "information_schema"."tables"
WHERE "table_schema" = '{table_schema}';
"""

schemaViews = """SELECT "table_name"
FROM "information_schema"."views"
WHERE "table_schema" = '{table_schema}';
"""

schemaMaterializedViews = """SELECT matviewname, definition
FROM pg_matviews
WHERE schemaname = '{table_schema}';"""

tableVews = """SELECT table_name, is_updatable,
        is_insertable_into, 
        obj_description('{table_schema}.{table_name}'::regclass)
FROM information_schema.views
WHERE table_schema = '{table_schema}'
        AND table_name = '{table_name}';"""


# Dados da tabela
schemaTabelaColuna = """SELECT COALESCE( col.column_name, '' )
, COALESCE( col.data_type , '' ),
COALESCE( col.is_nullable, '' ),
COALESCE( col.column_default, '' ),
COALESCE( pg_catalog.col_description( pgc.oid, col.ordinal_position ), '' )
FROM information_schema.columns col
INNER JOIN pg_catalog.pg_class pgc
        ON pgc.relname = col.table_name
INNER JOIN pg_catalog.pg_constraint con
        ON con.conrelid = pgc.oid

WHERE col.table_schema = '{table_schema}'
AND col.table_name = '{table_name}'
ORDER BY col.ordinal_position;"""

schemaViewColuna = """SELECT col.column_name
, (CASE WHEN col.data_type = 'USER-DEFINED'
        THEN col.udt_name
ELSE col.data_type
END),
col.is_nullable,
col.column_default
FROM information_schema.columns col

WHERE col.table_schema = '{table_schema}'
AND col.table_name = '{table_name}'
ORDER BY col.ordinal_position;"""

schemaMaterializedViewColuna = """SELECT a.attname,
pg_catalog.format_type(a.atttypid, a.atttypmod),
a.attnotnull
FROM pg_attribute a
JOIN pg_class t on a.attrelid = t.oid
JOIN pg_namespace s on t.relnamespace = s.oid
WHERE a.attnum > 0 
AND NOT a.attisdropped
AND t.relname = '{table_name}'
AND s.nspname = '{table_schema}'
ORDER BY a.attnum;"""

tableIndexes = """SELECT indexname, indexdef
FROM pg_indexes
WHERE schemaname = '{table_schema}'
AND tablename = '{table_name}';"""


tableConstraint = """SELECT con.conname, 
    (CASE WHEN "con"."contype" = 'p' THEN 'Primary Key'
    WHEN "con"."contype" = 'f' THEN 'Foreign Key'
    WHEN "con"."contype" = 'c' THEN 'Check Constraint'
    WHEN "con"."contype" = 'u' THEN 'Unique Constraint'
    WHEN "con"."contype" = 't' THEN 'Trigger Constraint'
    WHEN "con"."contype" = 'x' THEN 'Exclusion Constraint'
    END) AS "tipo",
    att.attname

FROM pg_catalog.pg_constraint con -- Restrições
    INNER JOIN pg_catalog.pg_class rel -- dados da tabela
        ON rel.oid = con.conrelid
    INNER JOIN pg_catalog.pg_namespace nsp -- dados do schema
        ON nsp.oid = con.connamespace 
    INNER JOIN pg_attribute att
        ON att.attrelid = con.conindid -- dados da coluna
WHERE nsp.nspname = '{table_schema}'
    AND rel.relname = '{table_name}';"""
    
tableTriggers = """SELECT  trigger_name ,
        trigger_schema ,
        (action_timing || ' ' || event_manipulation ),
        action_orientation
--, *
FROM information_schema.triggers
WHERE event_object_schema = '{table_schema}'
AND event_object_table = '{table_name}';"""

tableDadosGeo = """SELECT "gec"."type", 
        ( "srs"."auth_name" || ' ' || "auth_srid" ),
        "coord_dimension", 
        "f_geometry_column"
--, gec.*
FROM "geometry_columns" "gec"
LEFT JOIN "spatial_ref_sys" "srs" ON "gec"."srid" = "srs"."srid"
WHERE "f_table_schema" = '{table_schema}'
AND "f_table_name" = '{table_name}'
;"""
tableExtent = """SELECT ST_XMIN( ST_Extent( "gtb"."{col_geom}" ) ), 
        ST_YMIN( ST_Extent( "gtb"."{col_geom}" ) ) , 
        ST_XMAX( ST_Extent( "gtb"."{col_geom}" ) ), 
        ST_YMAX( ST_Extent( "gtb"."{col_geom}" ) )
FROM "{table_schema}"."{table_name}" "gtb";"""

dbVersion = """SELECT version(), 
PostGIS_lib_version(), 
PostGIS_GEOS_Version(), PostGIS_PROJ_Version();"""

schemaTriggers = """SELECT trigger_name, 
        event_manipulation, 
        event_object_schema, 
        event_object_table, 
        action_statement, 
        action_orientation, 
        action_timing
--, *
FROM information_schema.triggers
WHERE trigger_schema = '{table_schema}'
ORDER BY trigger_schema, trigger_name;"""

viewDefinition = """select definition
FROM pg_views
where viewname = '{table_name}' and schemaname = '{table_schema}';"""