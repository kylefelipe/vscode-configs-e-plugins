# -*- coding: utf-8 -*-

import psycopg2
import json
import auxiliar
import os 
from configure import config
from monta_tabela import gravaPaginaTabela
from monta_schema import gravaPaginaSchema
from monta_views import gravaPaginaView
from monta_readme_schemas import gravaReadmeSchema
from monta_readme_tabelas import gravaReadmeTabela
from monta_readme_views import gravaReadmeViews
from monta_readme_triggers import gravaReadmeTriggerMd
from monta_materialized import gravaPaginaMatView
from monta_readme_materialized_views import gravaReadmeMaterializesViews
from monta_index import gravaIndex
from conecta_banco import *
import consultas

# Busca a pasta de documentação no arquivo parametros.ini
base = config(section='caminho_base')['docs']

#consulta = getSchemas.format()
consuta = getSchemas.format(schemas_exclude = config(section='schemas_exclude')['schemas'])
schemas = [schema[0] for schema in connect(consuta, 1)]

# Removi topology , 'delimitacao_varjota' 'ustda' 'rde_rmf_rmc' 'empreendimentos'
# 'plano_contingencia', 'sefin_fortaleza'

gravaReadmeSchema(caminho=base, lista_schemas=schemas)
gravaReadmeTabela(caminho=base, lista_schemas=schemas)
gravaReadmeViews(caminho=base, lista_schemas=schemas)
gravaReadmeMaterializesViews(caminho=base, lista_schemas=schemas)
gravaReadmeTriggerMd(caminho=base, schema=schemas)

for schema in schemas:
    gravaPaginaSchema(caminho=base, table_schema=schema)
    
    tabelas = connect(schemaTables.format( table_schema=schema), 1)
    for tabela in tabelas:
        gravaPaginaTabela(caminho=base, table_schema=schema,
                          table_name=tabela[ 0 ], table_type=0)

    views = connect(schemaViews.format(table_schema=schema), 1)
    for view in views:
        gravaPaginaView(caminho=base, table_schema=schema,
                          table_name=view[ 0 ])
        
    materialized_views = connect(schemaMaterializedViews.format(table_schema=schema), 1)
    for materialized_view in materialized_views:
        gravaPaginaMatView(caminho=base, table_schema=schema, 
                          table_name=materialized_view[ 0 ])

gravaIndex(caminho=base)

print( 'ok' )
