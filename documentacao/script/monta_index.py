# -*- coding: utf-8 -*-

import os
import auxiliar
from conecta_banco import connect, connectionSettings
from consultas import dbVersion

def geraVersao() -> str:
    versao = connect( dbVersion, 2 )
    dadoMd = """# Banco de dados IGEO\n"""
    dadoMd += """\nDocumentação do Banco de dados do IGEO  """
    dadoMd += f"""\n\n## Dados PostgreSQL"""
    dadoMd += f"""\n__Nome do Banco:__ ```{connectionSettings['database']}```  """
    dadoMd += f"""\n__Máquina:__ ```{connectionSettings['host']}```  """
    dadoMd += f"""\n__Porta:__ ```{connectionSettings['port']}```  """
    dadoMd += f"""\n__Versão:__ ```{versao[0]}```  """
    dadoMd += """\n__Table Spaces:__  """
    dadoMd += """\n* Tabelas, Views, Materialized Views: ts_pgis_dados  """
    dadoMd += """\n* Índices: ts_pgis_index  """
    dadoMd += f"""\n\n## Dados Postgis"""
    dadoMd += f"""\n\n__Versão:__ ```{versao[1]}```  """
    dadoMd += f"""\n__GEOS:__ ```{versao[2]}```  """
    dadoMd += f"""\n__PROJ:__ ```{versao[3]}```  """
    return dadoMd

def gravaIndex(caminho: str):
    arquivo = os.path.join( caminho , 'README.md' )
    with open( arquivo, 'w' ) as f:
        f.write( geraVersao() )
    print( "Arquivo Indice Gerado" )
