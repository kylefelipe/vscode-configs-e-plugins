# -*- coding: utf-8 -*-

import os
import auxiliar
from conecta_banco import connect
from consultas import *

def geraCabecalho( table_schema: str , table_name: str ) -> str:
    print( "Cabeçalho da Materialized View {table_schema}.{table_name}".format( table_schema=table_schema, table_name=table_name ))
     
    consulta = dadoGeo(table_schema=table_schema , table_name=table_name)

    dadoMd = f"# {table_name}\n"
    dadoMd += f"""\nSchema: ```{table_schema}```  """
    dadoMd += """\nProprietário: ```ro_pgis```  """
    if consulta is None:
        dadoMd += """\nTipo: ```Tabular```  """
    
    return dadoMd

def dadoGeo( table_schema: str, table_name: str ) -> str:
    consulta = connect(tableDadosGeo.format(table_schema=table_schema, table_name=table_name), 2)
    if consulta is None:
        return None
    else:
        extent = connect(tableExtent.format(table_schema=table_schema, table_name=table_name,
                                            col_geom=consulta[3]), 2 )
        dadoMd = f"""\nTipo: ```{consulta[0]}```  """
        dadoMd += f"""\nRef. Espacial: ```{consulta[1]}```  """
        dadoMd += f"""\nDimensão: ```{consulta[2]}```  """
        dadoMd += f"""\nExtensão Espacial:  """
        dadoMd += f"""\n* X Min = ```{extent[0]}```  """
        dadoMd += f"""\n* Y Min = ```{extent[1]}```  """
        dadoMd += f"""\n* X Max = ```{extent[2]}```  """
        dadoMd += f"""\n* X Max = ```{extent[3]}```  """
        dadoMd += '\n'
        return dadoMd

def geraDescricao( table_schema , table_name ) -> str :
    """Pegar os comentarios da tabela e gera a descrição."""
    print( "Descrição da Materialized View {table_schema}.{table_name}".format( table_schema=table_schema, table_name=table_name ))
    dados = connect(f"SELECT obj_description( '{table_schema}.{table_name}'::regclass );" , 2 ) 
    print(dados)

    dadoMd = "\n## Descrição\n"

    for descricao in dados:
        if descricao is None:
            dadoMd += '\n__Sem descrição.__'
        else:
            dadoMd += '\n' + descricao
            
    return dadoMd

def geraCampos( table_schema, table_name ) -> str:
    
    dados = connect(schemaMaterializedViewColuna.format( table_schema=table_schema 
                                             , table_name=table_name ) , 1 )
    dadoMd = "\n# Campos\n"

    dadoMd += "\n| ATRIBUTO | TIPO | Null |"
    dadoMd += "\n|:---------|:-----|:-----|"
    if dados is None:
        dadoMd = ''
    else:
        for dado in dados:
            dadoMd =  dadoMd + "\n|{}|{}|{}|".format( *dado )
        
    return dadoMd

def geraDefinition(table_schema : str , table_name : str ) -> str:
    
    consulta =  connect(f"""SELECT definition
    FROM pg_matviews
    WHERE schemaname = '{table_schema}'
        AND matviewname = '{table_name}';""" , 2 )[0]
    
    arquivoMd = """\n\n## Definição da Materialized View\n"""
    arquivoMd += """```sql\n"""
    arquivoMd += consulta
    arquivoMd += """\n```"""
        
    return arquivoMd

def geraPaginaMatView( table_schema : str , table_name : str ) -> str:
    arquivoMd = geraCabecalho(table_schema=table_schema, table_name=table_name)
    arquivoMd += geraDescricao(table_schema=table_schema, table_name=table_name)
    arquivoMd += geraCampos(table_name=table_name, table_schema=table_schema)
    arquivoMd += geraDefinition(table_name=table_name, table_schema=table_schema)
    return arquivoMd


def gravaPaginaMatView( caminho : str , table_schema : str , table_name : str ) -> bool:
    """Grava a página da tabela na pasta devida
    caminho: str - caminho da pasta docs."""
    print(f"Gravando MD da tabela {table_name}")
    arquivo = os.path.join(caminho, 'materialized_views', table_name.lower()+'.md' )
    with open(arquivo, 'w' ) as f:
        f.write(geraPaginaMatView(table_schema=table_schema, table_name=table_name))
    return True
