# -*- coding: utf-8 -*-

import os
import auxiliar
from conecta_banco import connect
from consultas import *

def montaMaterializedView(table_schema: str ) -> str:
    dadoMod = ""
    dadoMod += """\n| MATERIALIZED VIEW | DESCRIÇÃO |"""
    dadoMod += """\n|:------------------|:----------|"""
    tabelas = connect( schemaMaterializedViews.format( table_schema=table_schema ) , 1)
    if len(tabelas) > 0:
        for tabela in tabelas:
            descricao = connect(f"SELECT obj_description( '{table_schema}.{tabela[0]}'::regclass );",  1 )
            descript = ""
            for dsc in descricao:
                if dsc[0] is not None:
                    descript = descript + dsc[ 0 ].replace('\n', ' ')
                else:
                    descript = "__SEM DESCRIÇÃO__"
            dadoMod += f"""\n|[{tabela[0]}](../materialized_views/{tabela[0]}.md)|{descript}|"""
    else:
        dadoMod += """\n__SEM VIEWS__"""
    return dadoMod

def geraReadmeMaterializeView(lista_schemas: list) -> str:

    dadoMod = """# MATERIALIZED VIEWS do IGEO\n"""
    dadoMod += """Informações das MATERIALIZED VIEWS do projeto IGEO e suas descrições\n\n"""
    dadoMod += """__Lista de MATERIALIZED VIEWS do projeto__\n"""

    for schema in lista_schemas:
        dadoMod += f"""\n\n### {schema.upper()}\n"""
        dadoMod += montaMaterializedView(table_schema=schema)
    return dadoMod

def gravaReadmeMaterializesViews( caminho: str , lista_schemas: list ):
    print( "Gerando MD MATERIALIZED VIEWS!" )
    arquivo = os.path.join( caminho , 'materialized_views' , 'README.md' )
    
    with open( arquivo, 'w' ) as f:
        f.write(geraReadmeMaterializeView( lista_schemas=lista_schemas ) )
    print( "Arquivo Gerado" )

