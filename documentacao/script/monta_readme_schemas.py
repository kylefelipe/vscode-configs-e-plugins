# -*- coding: utf-8 -*-

import os
import auxiliar
from conecta_banco import connect
from consultas import *


def montaSchema(table_schema: str ) -> str:
    descricao = connect(schemaDescription.format(table_schema=table_schema), 2)
    dadoMd = f"\n## SCHEMA {table_schema.upper()}\n"
    if descricao is not None:
        dadoMd += f"""\n{descricao[1]}\n"""
    else:
        dadoMd += f"""\n__SEM DESCRIÇÃO__\n"""
    dadoMd += f"""\n* [TABELAS](../tables/README.md#{table_schema})"""
    dadoMd += f"""\n* [VIEWS](../views/README.md#{table_schema})"""
    dadoMd += f"""\n* [VIEWS MATERIALIZADAS](../materialized_views/README.md#{table_schema})"""
    dadoMd += f"""\n* [TRIGGERS](../triggers/README.md#{table_schema})\n"""
    return dadoMd

def montaMdSchemas( lista_schemas: list) -> str:
    dadoMd = """# SCHEMAS do IGEO\n"""
    dadoMd += """Informações dos SCHEMAS do projeto IGEO e suas descrições\n\n"""
    dadoMd += """__Lista de TABELAS do projeto__\n"""
    for schema in lista_schemas:
        dadoMd += montaSchema( table_schema=schema)
    return dadoMd

def gravaReadmeSchema( caminho: str , lista_schemas: list ):
    print( "Gerando MD SCHEMA!" )
    arquivo = os.path.join( caminho , 'schemas' , 'README.md' )
    with open( arquivo, 'w' ) as f:
        f.write(montaMdSchemas(lista_schemas=lista_schemas))
    print( "Arquivo Gerado" )
