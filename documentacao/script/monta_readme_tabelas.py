# -*- coding: utf-8 -*-

import os
import auxiliar
from conecta_banco import connect
from consultas import *

def montaTabela(table_schema: str ) -> str:
    dadoMod = ""
    dadoMod += """\n| TABELAS | DESCRIÇÃO |"""
    dadoMod += """\n|:--------|:----------|"""
    tabelas = connect( schemaTables.format( table_schema=table_schema ) , 1)
    if len(tabelas) > 0:
        for tabela in tabelas:
            descricao = connect(f"SELECT obj_description( '{table_schema}.{tabela[0]}'::regclass );",  1 )
            descript = ""
            for dsc in descricao:
                if dsc[0] is not None:
                    descript = descript + dsc[ 0 ].replace('\n', ' ')
                else:
                    descript = "__SEM DESCRIÇÃO__"
            dadoMod += f"""\n|[{tabela[0]}](../tables/{tabela[0]}.md)|{descript}|"""
    else:
        dadoMod += """\n__Sem Tabelas__"""
    return dadoMod

def geraReadmeTabela( lista_schemas: list ) -> str:

    dadoMod = """# TABELAS do IGEO\n"""
    dadoMod += """Informações das TABELAS do projeto IGEO e suas descrições\n\n"""
    dadoMod += """__Lista de TABELAS do projeto__\n"""

    for schema in lista_schemas:
        dadoMod += f"""\n\n### {schema.upper()}\n"""
        dadoMod += montaTabela(table_schema=schema)
    return dadoMod

def gravaReadmeTabela( caminho: str , lista_schemas: list ):
    print( "Gerando MD TABELAS!" )
    arquivo = os.path.join( caminho , 'tables' , 'README.md' )
    
    with open( arquivo, 'w' ) as f:
        f.write( geraReadmeTabela( lista_schemas=lista_schemas ) )
    print( "Arquivo Gerado" )
