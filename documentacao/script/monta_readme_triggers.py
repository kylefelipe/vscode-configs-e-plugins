# -*- coding: utf-8 -*-

import os
import auxiliar
from conecta_banco import connect
from consultas import *
#from monta_tabela import geraGatilhos

def geraGatilhoMd( schema: list ) -> str:
    
    dadoMd = """# TRIGGERS\n"""
    for table_schema in schema:
        dadoMd += f"""\n## SCHEMA {table_schema}"""
        triggers = connect(schemaTriggers.format(table_schema=table_schema), 1)
        if len(triggers) == 0:
            dadoMd += """\n__SEM TRIGGER__"""
        else:
            dadoMd += """\n| NOME | AÇÃO | SCHEMA | TABELA | PROCEDIMENTO | ORIENTAÇÃO | TEMPO |"""
            dadoMd += """\n|:-----|:-----|:-------|:-------|:-------------|:-----------|:------|"""
            for resposta in triggers:
                if resposta is None:
                    dadoMd += """\n__SEM TRIGGER__"""
                else:
                    dadoMd += f"""\n|{resposta[0]}|{resposta[1]}|[{resposta[2]}](../schemas/{resposta[2]}.md)"""
                    dadoMd += f"""|[{resposta[3]}](../tables/{resposta[3]}.md)|{resposta[4]}|{resposta[5]}|{resposta[6]}|"""
        dadoMd += "\n"

    return dadoMd

def gravaReadmeTriggerMd ( caminho: str, schema: list):
    arquivo = os.path.join(caminho, 'triggers', 'README.md' )
    with open(arquivo, 'w') as file:
        file.write(geraGatilhoMd(schema))
    print( 'Documento de gatilhos gerado com sucesso!' )
