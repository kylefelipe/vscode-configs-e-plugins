# -*- coding: utf-8 -*-

import os
import auxiliar
from conecta_banco import connect
from consultas import *


def geraCabecalho( table_schema ) -> str:
    dadoMd = """# Schema  \n"""
    dadoMd += f"""\nNome do Schema: ```{table_schema}```"""
    owner = connect( schema_owner.format( table_schema=table_schema ) , 2 )
    dadoMd += f"""\nProprietário: ```{owner[ 1 ]}```"""
    descricao = connect( schemaDescription.format( table_schema=table_schema ) , 1 )
    dadoMd += """\n\n## Descrição\n"""
    for desc in descricao:
        if desc[ 0 ] is not None:
            dadoMd += '\n' + desc[ 1 ]
        
    return dadoMd

def geraListaTabelas( table_schema:str ) -> str:
    
    dadoMd = """\n## Tabelas\n"""
    dadoMd += """\n| TABELAS DO SCHEMA | DESCRIÇÃO |"""
    dadoMd += """\n|:------------------|:----------|"""
    tabelas = connect( schemaTables.format( table_schema=table_schema ) ,  1 )
    
    for tabela in tabelas:
        if tabela[ 0 ] is not None:
            descricao = connect(f"SELECT obj_description( '{table_schema}.{tabela[ 0 ]}'::regclass );",  1 )
            descript = ""
            for dsc in descricao:
                if dsc[ 0 ] is not None:
                    descript = descript + dsc[ 0 ].replace('\n', ' ')
                else:
                    descript = "__SEM DESCRIÇÃO__"
            dadoMd += f"""\n|[{tabela[ 0 ]}](../tables/{tabela[ 0 ]}.md)|{descript}|"""
            # dadoMd += f"""\n|{tabela[ 0 ]}|"""
        else:
            dadoMd += """\n__Sem Tabelas__"""
    return dadoMd

def geraPaginaSchema( table_schema ) -> str:
    dadoMd = geraCabecalho( table_schema=table_schema ) + '\n'
    dadoMd += geraListaTabelas( table_schema=table_schema ) + '\n'
    return dadoMd

def gravaPaginaSchema( caminho: str , table_schema ):
    print( f"Gravando MD do schema { table_schema }")
    arquivo = os.path.join( caminho,
                           'schemas', table_schema.lower()+'.md' )
    with open( arquivo, 'w' ) as f:
        f.write( geraPaginaSchema( table_schema=table_schema ) )
    print( "Arquivo Gerado!" )
