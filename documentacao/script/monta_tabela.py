# -*- coding: utf-8 -*-

import os
import auxiliar
from conecta_banco import connect
from consultas import *

def geraCabecalho( table_schema: str , table_name: str ) -> str:
    print( "Cabeçalho da Tabela {table_schema}.{table_name}".format( table_schema=table_schema, table_name=table_name ))
     
    consulta = dadoGeo(table_schema=table_schema , table_name=table_name)

    dadoMd = f"# {table_name}\n"
    dadoMd += f"""\nSchema: ```{table_schema}```  """
    dadoMd += """\nProprietário: ```ro_pgis```  """
    if consulta is None:
        dadoMd += """\nTipo: ```Tabular```  """
    else:
        dadoMd += consulta

    return dadoMd

def dadoGeo( table_schema: str, table_name: str ) -> str:
    consulta = connect(tableDadosGeo.format(table_schema=table_schema, table_name=table_name), 2)
    if consulta is None:
        return None
    else:
        extent = connect(tableExtent.format(table_schema=table_schema, table_name=table_name,
                                            col_geom=consulta[3]), 2 )
        dadoMd = f"""Tipo: ```{consulta[0]}```  """
        dadoMd += f"""\nRef. Espacial: ```{consulta[1]}```  """
        dadoMd += f"""\nDimensão: ```{consulta[2]}```  """
        dadoMd += f"""\nExtensão Espacial:  """
        dadoMd += f"""\n* X Min = ```{extent[0]}```  """
        dadoMd += f"""\n* Y Min = ```{extent[1]}```  """
        dadoMd += f"""\n* X Max = ```{extent[2]}```  """
        dadoMd += f"""\n* X Max = ```{extent[3]}```  """
        dadoMd += '\n'
        return dadoMd

def geraDescricao( table_schema , table_name ) -> str :
    """Pegar os comentarios da tabela e gera a descrição."""
    print( "Descrição da Tabela {table_schema}.{table_name}".format( table_schema=table_schema, table_name=table_name ))
    dados = connect(f"SELECT obj_description( '{table_schema}.{table_name}'::regclass );" , 1 ) 

    dadoMd = "## Descrição\n"

    for descricao in dados:
        if descricao[ 0 ] is None:
            dadoMd += '\n__Sem descrição de tabela.__'
        else:
            dadoMd += '\n' + descricao[ 0 ]
            
    return dadoMd


def geraCampos( table_schema, table_name ) -> str:
    
    dados = connect(schemaTabelaColuna.format( table_schema=table_schema 
                                             , table_name=table_name ) , 1 )
    dadoMd = "## Campos\n"

    dadoMd += "\n| ATRIBUTO | TIPO | Null | PADRÃO | DESCRICAO |"
    dadoMd += "\n|:---------|:-----|:-----|:-------|:----------|"
    if dados is None:
        dadoMd = ''
    else:
        for dado in dados:
            dadoMd += "\n|{}|{}|{}|{}|{}|".format( *dado )
        
    return dadoMd

def geraRestricoes( table_schema , table_name ) -> str:
    dados = connect( tableConstraint.format( table_schema=table_schema
                                            , table_name=table_name ) , 1 ) 
    dadoMd = """| NOME | TIPO | COLUNA |\n|:----|:----|:------|"""
    if dados is None:
        dadoMd = '__SEM RESTRIÇÔES__'
    else:
        for dado in dados:
            dadoMd += '\n' + "|{}|{}|{}|".format( *dado )
        
    return dadoMd

def geraIndices( table_schema , table_name ):
    dados = connect( tableIndexes.format( table_schema=table_schema , 
                                         table_name=table_name) , 1 )
    dadoMd = """| NOME | DEFINIÇÂO |\n|:----|:------|"""
        
    if dados is None:
        dadoMd = '__SEM ÍNDICES__'
    else:
        for dado in dados:
            dadoMd += "\n|{}|{}|".format( *dado )
            
    return dadoMd

def geraGatilhos( table_schema , table_name ):
    dados = connect( tableTriggers.format( table_schema=table_schema , 
                                         table_name=table_name) , 1 )
    dadoMd = """| NOME | SCHEMA do TRIGGER  | TIPO | ORIENTAÇÃO |\n|:----|:------|:----|:----------|"""
            
    if dados is None:
        dadoMd = '__SEM GATILHOS__'
    else:
        for dado in dados:
            dadoMd += "\n|{}|{}|{}|{}|".format( *dado )

    return dadoMd

def geraPaginaTabela( table_schema : str , table_name : str ) -> str:

    montagem = [geraCabecalho( table_schema, table_name ),
                geraDescricao( table_schema , table_name ), 
                geraCampos( table_schema , table_name ) ,
                geraRestricoes( table_schema , table_name ), 
                geraIndices( table_schema , table_name ),
                geraGatilhos( table_schema , table_name ) ]

    arquivoMd = auxiliar.tabela.format( *montagem )
    return arquivoMd

def gravaPaginaTabela( caminho : str, table_type: int , table_schema : str , table_name : str ) -> bool:
    """Grava a página da tabela na pasta devida
    caminho: str - caminho da pasta docs
    table_type: int - 0 para tabela, 1 view, 2 materialized view."""
    print(f"Gravando MD da tabela {table_name}")
    tableType = ['tables', 'views', 'materialized_views', ]
    arquivo = os.path.join(caminho, tableType[table_type],table_name.lower()+'.md' )
    with open(arquivo, 'w' ) as f:
        f.write(geraPaginaTabela(table_schema=table_schema, table_name=table_name))
    return True
