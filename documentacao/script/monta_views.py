# -*- coding: utf-8 -*-

import os
import auxiliar
from conecta_banco import connect
from consultas import *

def geraCabecalho( table_schema: str , table_name: str ) -> str:
    print( "Cabeçalho da View {table_schema}.{table_name}".format( table_schema=table_schema, table_name=table_name ))
     
    consulta = dadoGeo(table_schema=table_schema , table_name=table_name)

    dadoMd = f"# {table_name}\n"
    dadoMd += f"""\nSchema: ```{table_schema}```  """
    dadoMd += """\nProprietário: ```ro_pgis```  """
    if consulta is None:
        dadoMd += """\nTipo: ```Tabular```  """
    else:
        dadoMd += consulta

    return dadoMd

def dadoGeo( table_schema: str, table_name: str ) -> str:
    consulta = connect(tableDadosGeo.format(table_schema=table_schema, table_name=table_name), 2)
    if consulta is None:
        return None
    else:
        extent = connect(tableExtent.format(table_schema=table_schema, table_name=table_name,
                                            col_geom=consulta[3]), 2 )
        dadoMd = f"""\nTipo: ```{consulta[0]}```  """
        dadoMd += f"""\nRef. Espacial: ```{consulta[1]}```  """
        dadoMd += f"""\nDimensão: ```{consulta[2]}```  """
        dadoMd += f"""\nExtensão Espacial:  """
        dadoMd += f"""\n* X Min = ```{extent[0]}```  """
        dadoMd += f"""\n* Y Min = ```{extent[1]}```  """
        dadoMd += f"""\n* X Max = ```{extent[2]}```  """
        dadoMd += f"""\n* X Max = ```{extent[3]}```  """
        dadoMd += '\n'
        return dadoMd
    
def geraDescricao( table_schema , table_name ) -> str :
    """Pegar os comentarios da tabela e gera a descrição."""
    print( "Descrição da View {table_schema}.{table_name}".format( table_schema=table_schema, table_name=table_name ))
    dados = connect(f"SELECT obj_description( '{table_schema}.{table_name}'::regclass );" , 1 ) 

    dadoMd = "\n## Descrição\n"

    for descricao in dados:
        if descricao[0] is None:
            dadoMd += '\n__Sem descrição de tabela.__'
        else:
            dadoMd += '\n' + descricao[ 0 ]
            
    return dadoMd+'\n'

def geraCampos( table_schema, table_name ) -> str:
    
    dados = connect(schemaViewColuna.format( table_schema=table_schema 
                                             , table_name=table_name ) , 1 )
    dadoMd = "\n## Campos\n"

    dadoMd += "\n| ATRIBUTO | TIPO | Null | PADRÃO "
    dadoMd += "\n|:---------|:-----|:-----|:-------|"
    if dados is None:
        dadoMd = ''
    else:
        for dado in dados:
            dadoMd =  dadoMd + "\n|{}|{}|{}|{}|".format( *dado )
        
    return dadoMd

def pegaViews(table_schema: str) -> list:
    consulta = connect(tableVews.format(table_schema=table_schema), 1)
    return consulta

def geraPaginaView( table_schema: str, table_name: str) -> str:
    # dadoView = f"""### {table_name}\n"""
    # description = connect( f"""select obj_description('rde.ete_estacao_tratamento_esgoto_mto'::regclass);""", 2)
    # dadoView = dadoView + """ """
    
    arquivoMd = geraCabecalho(table_schema=table_schema, table_name=table_name)
    arquivoMd += geraDescricao(table_schema=table_schema, table_name=table_name)
    arquivoMd += geraCampos(table_name=table_name, table_schema=table_schema)
    arquivoMd += geraDefinition(table_name=table_name, table_schema=table_schema)
    return arquivoMd

def geraDefinition(table_schema: str, table_name: str) -> str:
    consulta = connect(viewDefinition.format(table_schema=table_schema, table_name=table_name), 2)
    arquivoMd = """\n\n## Definição da View\n"""
    arquivoMd += """```sql\n"""
    arquivoMd += consulta[0]
    arquivoMd += """\n```"""
    return arquivoMd

#arquivo = os.path.join(caminho)
def gravaPaginaView(caminho: str, table_schema: str, table_name: str):
    arquivo = os.path.join(caminho, 'views', table_name.lower()+'.md')
    with open(arquivo, 'w') as f:
        f.write(geraPaginaView(table_schema=table_schema, table_name=table_name))
    return True
    
#print(geraPaginaView(table_schema='mapas_geoserver', table_name='une_teste'))
