SELECT '"' || a.table_name || '"' || ' : ' || json_agg( a.column_name )
FROM information_schema.columns a
JOIN (
    SELECT tablename as tablename
    FROM pg_catalog.pg_tables
    WHERE schemaname != 'pg_catalog'
    AND schemaname != 'information_schema'
) b on a.table_name = b.tablename
WHERE table_schema in (
    'bac', 'bba', 'bme', 
    'bsa', 
    'empreendimentos', 
    'gcope', 
    'gefar', 
    'gemam', 
    'gomet', 
    'gproj', 
    'ibge', 
    'igeo_dev', 
    'mapas_calor', 
    'meio_ambiente', 
    'public', 
    'rda', 
    'rda_d', 
    'rde', 
    'testes', 
    'topografia', 
    'ustda', 
    'bsi', 
    'projetos', 
    'topology', 
    'carga', 
    'rde_rmf_rmc', 
    'agua_macro', 
    'plano_contingencia', 
    'sefin_fortaleza', 
    'ativos', 
    'semace', 
    'mapas_geoserver', 
    'relatorio_producao_igeo', 
    'contingencia', 
    'osm', 
    'delimitacao_varjota', 
    'pendencia_igeo', 
    'verificacao_relaorio_ses'
)
--AND table_name   = 'your_table'
GROUP BY a.table_name
    ;