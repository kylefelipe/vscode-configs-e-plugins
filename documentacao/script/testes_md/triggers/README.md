# TRIGGERS

## SCHEMA testes
| NOME | AÇÃO | SCHEMA | TABELA | PROCEDIMENTO | ORIENTAÇÃO | TEMPO |
|:-----|:-----|:-------|:-------|:-------------|:-----------|:------|
|auto_fill_quadra_in_linha|UPDATE|[testes](../schemas/testes.md)|[linha_lote](../tables/linha_lote.md)|EXECUTE PROCEDURE testes.auto_fill_quadra_in_linha()|ROW|BEFORE|
|auto_fill_quadra_in_linha|UPDATE|[testes](../schemas/testes.md)|[perimetro_referencia](../tables/perimetro_referencia.md)|EXECUTE PROCEDURE testes.auto_fill_quadra_in_linha()|ROW|BEFORE|
|auto_fill_quadra_in_linha|INSERT|[testes](../schemas/testes.md)|[perimetro_referencia](../tables/perimetro_referencia.md)|EXECUTE PROCEDURE testes.auto_fill_quadra_in_linha()|ROW|BEFORE|
|auto_fill_quadra_in_linha|INSERT|[testes](../schemas/testes.md)|[linha_lote](../tables/linha_lote.md)|EXECUTE PROCEDURE testes.auto_fill_quadra_in_linha()|ROW|BEFORE|
|data_atualizacao|UPDATE|[testes](../schemas/testes.md)|[geocoding_lotes_igeo](../tables/geocoding_lotes_igeo.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[testes](../schemas/testes.md)|[linha_lote](../tables/linha_lote.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_cadastro|INSERT|[testes](../schemas/testes.md)|[linha_lote](../tables/linha_lote.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[testes](../schemas/testes.md)|[geocoding_lotes_igeo](../tables/geocoding_lotes_igeo.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|insert_point_clients|DELETE|[testes](../schemas/testes.md)|[perimetro_referencia](../tables/perimetro_referencia.md)|EXECUTE PROCEDURE testes.insert_point_clients()|ROW|AFTER|
|insert_point_clients|INSERT|[testes](../schemas/testes.md)|[perimetro_referencia](../tables/perimetro_referencia.md)|EXECUTE PROCEDURE testes.insert_point_clients()|ROW|AFTER|
|insert_point_clients|INSERT|[testes](../schemas/testes.md)|[linha_lote](../tables/linha_lote.md)|EXECUTE PROCEDURE testes.insert_point_clients()|ROW|AFTER|
|insert_point_clients|DELETE|[testes](../schemas/testes.md)|[linha_lote](../tables/linha_lote.md)|EXECUTE PROCEDURE testes.insert_point_clients()|ROW|AFTER|
|insert_point_clients|UPDATE|[testes](../schemas/testes.md)|[perimetro_referencia](../tables/perimetro_referencia.md)|EXECUTE PROCEDURE testes.insert_point_clients()|ROW|AFTER|
|insert_point_clients|UPDATE|[testes](../schemas/testes.md)|[linha_lote](../tables/linha_lote.md)|EXECUTE PROCEDURE testes.insert_point_clients()|ROW|AFTER|
|insert_point_clients_new|UPDATE|[testes](../schemas/testes.md)|[linha_lote](../tables/linha_lote.md)|EXECUTE PROCEDURE testes.insert_point_clients_new()|ROW|AFTER|
|insert_point_clients_new|DELETE|[testes](../schemas/testes.md)|[linha_lote](../tables/linha_lote.md)|EXECUTE PROCEDURE testes.insert_point_clients_new()|ROW|AFTER|
|insert_point_clients_new|INSERT|[testes](../schemas/testes.md)|[linha_lote](../tables/linha_lote.md)|EXECUTE PROCEDURE testes.insert_point_clients_new()|ROW|AFTER|
|trig_insert_geocoding_table|INSERT|[testes](../schemas/testes.md)|[perimetro_referencia](../tables/perimetro_referencia.md)|EXECUTE PROCEDURE testes.insert_geocoding_table_old()|ROW|AFTER|
|trig_insert_geocoding_table|INSERT|[testes](../schemas/testes.md)|[linha_lote](../tables/linha_lote.md)|EXECUTE PROCEDURE testes.insert_geocoding_table_old()|ROW|AFTER|
|usuario_atualizacao|UPDATE|[testes](../schemas/testes.md)|[linha_lote](../tables/linha_lote.md)|EXECUTE PROCEDURE usuario_atualizacao()|ROW|BEFORE|
|usuario_cadastro|INSERT|[testes](../schemas/testes.md)|[linha_lote](../tables/linha_lote.md)|EXECUTE PROCEDURE usuario_cadastro()|ROW|BEFORE|

## SCHEMA public
| NOME | AÇÃO | SCHEMA | TABELA | PROCEDIMENTO | ORIENTAÇÃO | TEMPO |
|:-----|:-----|:-------|:-------|:-------------|:-----------|:------|
|auto_fill_quadra_in_lote|UPDATE|[public](../schemas/public.md)|[lot_lotes](../tables/lot_lotes.md)|EXECUTE PROCEDURE auto_fill_quadra_in_lote()|ROW|BEFORE|
|auto_fill_quadra_in_lote|INSERT|[public](../schemas/public.md)|[lot_lotes](../tables/lot_lotes.md)|EXECUTE PROCEDURE auto_fill_quadra_in_lote()|ROW|BEFORE|
|cadastra_geografia_logradouro|INSERT|[public](../schemas/public.md)|[log_logradouros](../tables/log_logradouros.md)|EXECUTE PROCEDURE cadastra_geografia_logradouro()|ROW|BEFORE|
|cadastra_geografia_logradouro|UPDATE|[public](../schemas/public.md)|[log_logradouros](../tables/log_logradouros.md)|EXECUTE PROCEDURE cadastra_geografia_logradouro()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[rod_rodovias](../tables/rod_rodovias.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[pee_perfil_esgoto](../tables/pee_perfil_esgoto.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[poc_pocos](../tables/poc_pocos.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[pov_pocos_visita](../tables/pov_pocos_visita.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[qua_quadricula](../tables/qua_quadricula.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[rea_rede_agua](../tables/rea_rede_agua.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[sud_sub_distrito](../tables/sud_sub_distrito.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[imo_imovel](../tables/imo_imovel.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[loc_localidades_cagece_point](../tables/loc_localidades_cagece_point.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[log_logradouros](../tables/log_logradouros.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[mun_municipios](../tables/mun_municipios.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[pea_peca_agua](../tables/pea_peca_agua.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[qua_quadras](../tables/qua_quadras.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[rec_relatorio_cadastro_quadras](../tables/rec_relatorio_cadastro_quadras.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[ree_rede_esgoto](../tables/ree_rede_esgoto.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[sco_setor_comercial](../tables/sco_setor_comercial.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[suq_subquadricula](../tables/suq_subquadricula.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[tre_trecho_rede_esgoto](../tables/tre_trecho_rede_esgoto.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[une_unidades_negocio](../tables/une_unidades_negocio.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[cae_caixa_esgoto](../tables/cae_caixa_esgoto.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[dis_distritos](../tables/dis_distritos.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[den_detalhe_no](../tables/den_detalhe_no.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[gdf_galeria_drenagem_fortaleza](../tables/gdf_galeria_drenagem_fortaleza.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[hid_hidrometro](../tables/hid_hidrometro.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[pre_pecas_esgoto](../tables/pre_pecas_esgoto.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[res_relatorio_cadastro_setor](../tables/res_relatorio_cadastro_setor.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[res_reservatorios](../tables/res_reservatorios.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_atualizacao|UPDATE|[public](../schemas/public.md)|[rea_registros_alteracao](../tables/rea_registros_alteracao.md)|EXECUTE PROCEDURE data_atualizacao()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[res_relatorio_cadastro_setor](../tables/res_relatorio_cadastro_setor.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[rea_registros_alteracao](../tables/rea_registros_alteracao.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[ree_rede_esgoto](../tables/ree_rede_esgoto.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[sco_setor_comercial](../tables/sco_setor_comercial.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[suq_subquadricula](../tables/suq_subquadricula.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[tre_trecho_rede_esgoto](../tables/tre_trecho_rede_esgoto.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[une_unidades_negocio](../tables/une_unidades_negocio.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[cae_caixa_esgoto](../tables/cae_caixa_esgoto.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[dis_distritos](../tables/dis_distritos.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[den_detalhe_no](../tables/den_detalhe_no.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[gdf_galeria_drenagem_fortaleza](../tables/gdf_galeria_drenagem_fortaleza.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[hid_hidrometro](../tables/hid_hidrometro.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[pre_pecas_esgoto](../tables/pre_pecas_esgoto.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[res_reservatorios](../tables/res_reservatorios.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[rod_rodovias](../tables/rod_rodovias.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[pov_pocos_visita](../tables/pov_pocos_visita.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[pee_perfil_esgoto](../tables/pee_perfil_esgoto.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[poc_pocos](../tables/poc_pocos.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[rea_rede_agua](../tables/rea_rede_agua.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[sud_sub_distrito](../tables/sud_sub_distrito.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[imo_imovel](../tables/imo_imovel.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[loc_localidades_cagece_point](../tables/loc_localidades_cagece_point.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[qua_quadricula](../tables/qua_quadricula.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[log_logradouros](../tables/log_logradouros.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[mun_municipios](../tables/mun_municipios.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[pea_peca_agua](../tables/pea_peca_agua.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[qua_quadras](../tables/qua_quadras.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|data_cadastro|INSERT|[public](../schemas/public.md)|[rec_relatorio_cadastro_quadras](../tables/rec_relatorio_cadastro_quadras.md)|EXECUTE PROCEDURE data_cadastro()|ROW|BEFORE|
|preenche_codigo_setor_comercial|UPDATE|[public](../schemas/public.md)|[sco_setor_comercial](../tables/sco_setor_comercial.md)|EXECUTE PROCEDURE preenche_codigo_setor_comercial()|ROW|BEFORE|
|preenche_codigo_setor_comercial|INSERT|[public](../schemas/public.md)|[sco_setor_comercial](../tables/sco_setor_comercial.md)|EXECUTE PROCEDURE preenche_codigo_setor_comercial()|ROW|BEFORE|
|tg_preenche_informacoes_quadra|INSERT|[public](../schemas/public.md)|[qua_quadras](../tables/qua_quadras.md)|EXECUTE PROCEDURE preenche_informacoes_quadra()|ROW|BEFORE|
|tg_preenche_informacoes_quadra|UPDATE|[public](../schemas/public.md)|[qua_quadras](../tables/qua_quadras.md)|EXECUTE PROCEDURE preenche_informacoes_quadra()|ROW|BEFORE|
|usuario_atualizacao|UPDATE|[public](../schemas/public.md)|[qua_quadras](../tables/qua_quadras.md)|EXECUTE PROCEDURE usuario_atualizacao()|ROW|BEFORE|
|usuario_atualizacao|UPDATE|[public](../schemas/public.md)|[log_logradouros](../tables/log_logradouros.md)|EXECUTE PROCEDURE usuario_atualizacao()|ROW|BEFORE|
|usuario_atualizacao|UPDATE|[public](../schemas/public.md)|[pov_pocos_visita](../tables/pov_pocos_visita.md)|EXECUTE PROCEDURE usuario_atualizacao()|ROW|BEFORE|
|usuario_atualizacao|UPDATE|[public](../schemas/public.md)|[rea_registros_alteracao](../tables/rea_registros_alteracao.md)|EXECUTE PROCEDURE usuario_atualizacao()|ROW|BEFORE|
|usuario_cadastro|INSERT|[public](../schemas/public.md)|[pov_pocos_visita](../tables/pov_pocos_visita.md)|EXECUTE PROCEDURE usuario_cadastro()|ROW|BEFORE|
|usuario_cadastro|INSERT|[public](../schemas/public.md)|[log_logradouros](../tables/log_logradouros.md)|EXECUTE PROCEDURE usuario_cadastro()|ROW|BEFORE|
|usuario_cadastro|INSERT|[public](../schemas/public.md)|[qua_quadras](../tables/qua_quadras.md)|EXECUTE PROCEDURE usuario_cadastro()|ROW|BEFORE|
|usuario_cadastro|INSERT|[public](../schemas/public.md)|[rea_registros_alteracao](../tables/rea_registros_alteracao.md)|EXECUTE PROCEDURE usuario_cadastro()|ROW|BEFORE|
